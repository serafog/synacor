#ifdef __arm__

namespace arm
{
	static constexpr bool canArmv7 = __ARM_ARCH >= 7;

	typedef std::uint32_t instr;
	constexpr int fp = 11, ip = 12, sp = 13, lr = 14, pc = 15; // TODO: make an enum
	constexpr instr movi(int rd, int val) // TODO: rename to move and use the enum to overload
	{
		if (val < 0 or val > 0xff)
			throw "invalid value in movi";
		return 0xe3a0'0000 | rd << 12 | val;
	}
	constexpr instr orri_hi(int rd, int val)
	{
		if (val < 0 or val > 0xff)
			throw "invalid value in orri_hi";
		return 0xe380'0c00 | rd << 12 | rd << 16 | val;
	}
	constexpr instr movr(int rd, int rs) { return 0xe1a0'0000 | rd << 12 | rs; }
	constexpr instr nop() { return movr(0, 0); }
	// detect CPU and use newer movw instruction when available
	constexpr instr movw(int rd, int val)
	{
		if (val < 0 or val > 0xffff)
			throw "invalid value in movw";
		return 0xe300'0000 | rd << 12 | (val & 0xf000) << 4 | (val & 0xfff);
	}
	enum condition: instr
	{
		EQ, NE, HS, LO, MI, PL, VS, VC, HI, LS, GE, LT, GT, LE, AL
	};
	static_assert(GT == 0b1100 and AL == 0b1110);
	enum operation: instr
	{
		AND, EOR, SUB, RSB, ADD, ADC, SBC, RSC, TST, TEQ, CMP, CMN, ORR, MOV, BIC, MVN
	};
	static_assert(ADD == 0b0100 and CMP == 0b1010 and ORR == 0b1100 and MVN == 0b1111);
	constexpr instr S = 1 << 20, op_I = 1 << 25;
	constexpr instr lsl(int shift)
	{
		return shift << 7 | 0b000 << 4;
	}
	constexpr instr op(condition cond, operation oper, int rd, int rn, int rs)
	{
		return cond << 28 | oper << 21 | rn << 16 | rd << 12 | rs
			| (oper >= TST and oper <= CMN) << 20; // force S for them
	}
	constexpr instr smulbb(int rd, int rs, int rm)
	{
		return 0xe160'0080 | rd << 16 | rs << 8 | rm;
	}
	constexpr instr udiv(int rd, int rn, int rm)
	{
		return 0xe730'f010 | rd << 16 | rm << 8 | rn;
	}
	constexpr instr mls(int rd, int rn, int rm, int ra)
	{
		return 0xe060'0090 | rd << 16 | ra << 12 | rm << 8 | rn;
	}
	constexpr instr push(std::initializer_list<int> regs)
	{
		instr i = 0xe92d'0000;
		for (int r: regs) i |= 1 << r;
		return i;
	}
	constexpr instr pop(std::initializer_list<int> regs)
	{
		instr i = 0xe8bd'0000;
		for (int r: regs) i |= 1 << r;
		return i;
	}
	constexpr instr ldr(int rd, int rbase, int off_b)
	{
		instr i = 0xe590'0000 | rbase << 16 | rd << 12;
		if (off_b < 0)
		{
			i &= ~(1 << 23); // clear the 'up=add' bit to mean 'down=substract'
			off_b = -off_b;
		}
		if (off_b >= 0x1000) // just 12 bits are available
			throw "byte offset overflow in ldr";
		return i | off_b;
	}
	constexpr instr ldrpc(int rd, int off_b)
	{
		return ldr(rd, pc, off_b - 8); // undo the pipeline delay on pc
	}
	constexpr instr ldrh(int rd, int rbase, int off_b)
	{
		// L=1, S=0, H=1, W=0, U=1
		instr i = 0xe1d0'00b0 | rd << 12 | rbase << 16;
		if (off_b < 0)
		{
			i &= ~(1 << 23); // clear the 'up=add' bit to mean 'down=substract'
			off_b = -off_b;
		}
		if (off_b >= 0x100) // just 8 bits are available
			throw "byte offset overflow in ldrh";
		return i | (off_b & 0xf0) << 4 | (off_b & 0xf);
	}
	constexpr instr strh(int rd, int rbase, int off_b)
	{
		// L=0
		const auto L = S;
		return ldrh(rd, rbase, off_b) ^ L; // clear the L bit
	}
	constexpr instr ld_I = 1 << 22;
	constexpr instr blx(int rm) { return 0xe12f'ff30 | rm; }
	constexpr instr bx(condition cond, int rm) { return 0x012f'ff10 | cond << 28 | rm; }
	constexpr instr b(condition cond, int off_i)
	{
		off_i -= 2; // undo the pipeline delay on pc
		if ((off_i & 0xff00'0000) != 0xff00'0000 and (off_i & 0xff00'0000) != 0)
			throw "instruction offset overflow in branch";
		return 0x0a00'0000 | cond << 28 | (off_i & 0xff'ffff);
	}
}

constexpr int rmemory = 0 + 4, rregs = 1 + 4, rcallbacks = 2 + 4, rmachine_code = 3 + 4, rmax = 4 + 4;
// 4, 5, 6 are the machine_code arguments: memory, regs, &callbacks
// 7 is the machine_code pointer
// 8 is max = 0x7fff
constexpr arm::instr push = arm::push({rmemory, rregs, rcallbacks, rmachine_code, rmax, arm::lr});

// function epilogue
constexpr arm::instr ret = arm::pop({rmemory, rregs, rcallbacks, rmachine_code, rmax, arm::pc});

struct code_generator
{
#if defined(__ARMEB__)
	static inline void writeIns(arm::instr *const p, const arm::instr i)
	{
		const arm::instr j =
			(i & 0xff00'0000) >> 24 |
			(i & 0x00ff'0000) >> 8 |
			(i & 0x0000'ff00) << 8 |
			(i & 0x0000'00ff) << 24;
		*p = j;
	}
#elif defined(__ARMEL__)
	static inline void writeIns(arm::instr *const p, const arm::instr i)
	{
		*p = i;
	}
#else
#error Unknown CPU endianness
#endif
	arm::instr * &code_p;

	void movWord(const int dst, const word val)
	{
		if (arm::canArmv7)
			writeIns(code_p++, arm::movw(dst, val));
		else
		{
			writeIns(code_p++, arm::movi(dst, val & 0xff));
			const int high = val >> 8;
			if (high) writeIns(code_p++, arm::orri_hi(dst, high));
		}
	}
	void readReg(const int dst, const word src)
	{
		if (src > max)
		{
			writeIns(code_p++, arm::ldrh(dst, rregs, (src - max - 1) * sizeof src));
		}
		else
		{
			movWord(dst, src);
		}
	}
	void writeReg(const word dst, const int src)
	{
		if (dst > max)
		{
			writeIns(code_p++, arm::strh(src, rregs, (dst - max - 1) * sizeof dst));
		}
	}
	void condJump(arm::condition cond, int extra, const word pc, const word w,
		const arm::instr *const cmp_instrs_begin = nullptr, const arm::instr *const cmp_instrs_end = nullptr)
	{
		const auto code_p_begin = code_p;
		// use register 0 so that it's the argument and result of check_jump_target
		// we don't need w in r0 if we aren't checking and it's an immediate value
		if (w > max || enableCheckJumpTarget) readReg(0, w);
		if (enableCheckJumpTarget)
		{
			constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, check_jump_target));
			writeIns(code_p++, l1);
			constexpr arm::instr b1 = arm::blx(arm::ip);
			writeIns(code_p++, b1);
		}

		// now that we may have tested the branch target, we can issue the comparison instructions, if any
		code_p = std::copy(cmp_instrs_begin, cmp_instrs_end, code_p);

		extra += code_p - code_p_begin;

		if (w > max)
		{
			static_assert(INSTR_PER_WORD * sizeof(arm::instr) == 1 << 6); // confirm lsl 6 works
			writeIns(code_p++, arm::op(cond, arm::ADD, 0, rmachine_code, 0) | arm::lsl(6));
			writeIns(code_p++, arm::bx(cond, 0));
		}
		else // for a branch to a known location, we don't need r0 anymore since we use the branch instruction
		{
			const int off_i = (w - pc) * INSTR_PER_WORD - extra;
			writeIns(code_p++, arm::b(cond, off_i));
		}
	}
	void ternary(word &i, const bool mask, const arm::instr oper1, const arm::instr oper2 = 0)
	{
		const word a = memory[++i];
		const word b = memory[++i];
		const word c = memory[++i];
		readReg(0, b);
		readReg(1, c);
		writeIns(code_p++, oper1);
		if (oper2) writeIns(code_p++, oper2);
		if (mask) writeIns(code_p++, arm::op(arm::AL, arm::AND, 0, 0, rmax));
		writeReg(a, 0);
	}
	void haltIf_err_halt()
	{
		constexpr arm::instr c1 = arm::op(arm::AL, arm::CMP, 0, 0, err_halt >> 8 | 0xc'00) | arm::op_I;
		writeIns(code_p++, c1);
		constexpr arm::instr r1 = (ret & 0xfff'ffff) | arm::EQ;
		writeIns(code_p++, r1);
	}

	void at(word &i)
	{
		const auto i_begin = i;
		const word w = memory[i];
		const auto code_p_begin = code_p;
		switch (w)
		{
			case i_halt: // 0 arguments: code size is INSTR_PER_WORD
			default: // special halt on invalid or unknown instruction
			{
				writeIns(code_p++, arm::movi(0, w == i_halt ? 0 : 0xf7)); // r0 = 0 or 0xf7
				writeIns(code_p++, ret);
				break;
			}
			case i_noop: // 0 arguments
				break;
			case i_in: // 1 argument: code size is 2 * INSTR_PER_WORD
			{
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, getchar));
				writeIns(code_p++, l1);
				// pass the program counter as the argument (in order to savestate on EOF)
				movWord(0, i);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				haltIf_err_halt();
				writeReg(memory[++i], 0);
				break;
			}
			case i_out: // 1 argument: code size is 2 * INSTR_PER_WORD
			{
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, putchar));
				writeIns(code_p++, l1);
				const word w = memory[++i];
				readReg(0, w);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				break;
			}
			case i_jmp: // 1 argument: code size is 2 * INSTR_PER_WORD
			{
				const auto pc = i;
				const word w = memory[++i];
				const int extra = 0;
				condJump(arm::AL, extra, pc, w);
				break;
			}
			case i_jt: // 2 arguments: code size is 3 * INSTR_PER_WORD
			case i_jf: // 2 arguments
			{
				const auto pc = i;
				const word a = memory[++i];
				const word b = memory[++i];
				const int extra = code_p - code_p_begin;
				arm::instr cmp_instrs[3];
				auto *cp = cmp_instrs; // code_p but advancing in the small local array
				code_generator{cp}.readReg(1, a); // 1 or 2 instructions
				// reuse the register encoding but actually make it read it as an immediate zero value:
				writeIns(cp++, arm::op(arm::AL, arm::CMP, 0, 1, 0) | arm::op_I);
				condJump(w == i_jf ? arm::EQ : arm::NE, extra, pc, b, cmp_instrs, cp);
				break;
			}
			case i_set: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				readReg(0, b);
				writeReg(a, 0);
				break;
			}
			case i_add: // 3 arguments: code size is 4 * INSTR_PER_WORD
			case i_mult: // 3 arguments
			case i_and: // 3 arguments
			case i_or: // 3 arguments
			case i_mod: // 3 arguments
				switch (w)
				{
				case i_add: ternary(i, true, arm::op(arm::AL, arm::ADD, 0, 0, 1)); break;
				case i_and: ternary(i, false, arm::op(arm::AL, arm::AND, 0, 0, 1)); break;
				case i_or: ternary(i, false, arm::op(arm::AL, arm::ORR, 0, 0, 1)); break;
				case i_mult: ternary(i, true, arm::smulbb(0, 0, 1)); break;
				case i_mod:
					if (arm::canArmv7)
						ternary(i, false,
							arm::udiv(3, 0, 1), // r3 = r0 / r1
							arm::mls(0, 1, 3, 0)); // r0 = r0 - r1 * r3
					else
						ternary(i, false,
							arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, modulo)),
							arm::blx(arm::ip));
					break;
				}
				break;
			case i_eq: // 3 arguments
			case i_gt: // 3 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				const word c = memory[++i];
				readReg(0, b);
				readReg(1, c);
				writeIns(code_p++, arm::op(arm::AL, arm::CMP, 0, 0, 1));
				auto cond0 = arm::NE, cond1 = arm::EQ;
				if (w == i_gt) cond0 = arm::LS, cond1 = arm::HI;
				writeIns(code_p++, arm::op(cond0, arm::MOV, 0, 0, 0) | arm::op_I);
				writeIns(code_p++, arm::op(cond1, arm::MOV, 0, 0, 1) | arm::op_I);
				writeReg(a, 0);
				break;
			}
			case i_not: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				readReg(0, b);
				constexpr arm::instr mn = arm::op(arm::AL, arm::MVN, 0, 0, 0);
				writeIns(code_p++, mn);
				constexpr arm::instr an = arm::op(arm::AL, arm::AND, 0, 0, rmax);
				writeIns(code_p++, an);
				writeReg(a, 0);
				break;
			}
			case i_push: // 1 argument
			{
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, stack_push));
				writeIns(code_p++, l1);
				const word a = memory[++i];
				readReg(0, a);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				break;
			}
			case i_pop: // 1 argument
			{
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, stack_pop));
				writeIns(code_p++, l1);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				const word a = memory[++i];
				writeReg(a, 0);
				haltIf_err_halt();
				break;
			}
			case i_call: // 1 argument
			{
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, stack_push));
				writeIns(code_p++, l1);
				const word pc = i, pc_next = i + 2;
				movWord(0, pc_next);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				const int extra = code_p - code_p_begin;
				condJump(arm::AL, extra, pc, memory[++i]);
				break;
			}
			case i_ret: // 0 arguments, code size is only INSTR_PER_WORD
			{
				// TODO: use Callbacks::stack_pop and halt if empty, if INSTR_PER_WORD allows enough space
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, stack_pop_or_halt));
				writeIns(code_p++, l1);
				constexpr arm::instr b1 = arm::blx(arm::ip); // this may call std::exit, i.e. halt if stack is empty
				writeIns(code_p++, b1);
				if (enableCheckJumpTarget)
				{
					constexpr arm::instr l2 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, check_jump_target));
					writeIns(code_p++, l2);
					writeIns(code_p++, b1); // this may call std::exit, it returns its argument (i.e. r0 is unchanged)
				}
				static_assert(INSTR_PER_WORD * sizeof(arm::instr) == 1 << 6); // confirm lsl 6 works
				constexpr arm::instr a1 = arm::op(arm::AL, arm::ADD, 0, rmachine_code, 0) | arm::lsl(6);
				writeIns(code_p++, a1);
				constexpr arm::instr b3 = arm::bx(arm::AL, 0);
				writeIns(code_p++, b3);
				break;
			}
			case i_rmem: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				if (b > max)
				{
					readReg(0, b);
					static_assert(sizeof *memory == 2); // confirm lsl(1) works
					constexpr arm::instr sh = arm::movr(0, 0) | arm::lsl(1);
					writeIns(code_p++, sh);
				}
				else // if b is an immediate value, we can precompute lsl 1
				{
					movWord(0, b << 1);
				}
				constexpr arm::instr ld = arm::ldrh(1, rmemory, 0) ^ arm::ld_I;
				writeIns(code_p++, ld);
				writeReg(a, 1);
				break;
			}
			case i_wmem: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				readReg(1, b);
				readReg(0, a);
				constexpr arm::instr sh = arm::movr(2, 0) | arm::lsl(1);
				writeIns(code_p++, sh);
				constexpr arm::instr st = arm::strh(1, rmemory, 2) ^ arm::ld_I;
				writeIns(code_p++, st);
				constexpr arm::instr l1 = arm::ldr(arm::ip, rcallbacks, offsetof(Callbacks, write_mem));
				writeIns(code_p++, l1);
				constexpr arm::instr b1 = arm::blx(arm::ip);
				writeIns(code_p++, b1);
				break;
			}
		}
		const auto code_p_end = code_p_begin + (i + 1 - i_begin) * INSTR_PER_WORD;
		// there will be a gap, write an AL branch to the next block
		// only if the instruction isn't a guaranteed jump elsewhere
		constexpr static std::initializer_list<word> always_jumps{i_halt, i_jmp, i_call, i_ret};
		// w > i_noop means halt with result 0xf7
		if (w <= i_noop and std::find(always_jumps.begin(), always_jumps.end(), w) == always_jumps.end())
		{
			condJump(arm::AL, code_p - code_p_begin, i_begin, i + 1);
		}
		assert(code_p <= code_p_end);
		// don't write a few arm::nop() to fill until the next instruction
		// while (code_p < code_p_end) writeIns(code_p++, arm::nop());
		// leaving a potential previous instruction in place
		// null instructions, 0x0000'0000 will work as nops too: andeq r0, r0, r0

		if (enableCheckJumpTarget)
		{
			instruction_start[i_begin] = true;
			// only set to false when we know we have overwritten their code:
			// minus 1 because code_p always points to the next written location, code_p - 1 is the last written
			const auto i_at_code_p_m1 = i_begin + (code_p - 1 - code_p_begin) / INSTR_PER_WORD;
			for (auto j = i_begin + 1; j <= i_at_code_p_m1; ++j)
			{
				instruction_start[j] = false;
			}
		}
	}

	static void detectCpu()
	{
	}

};

static void prepare_machine_code(word pc)
{
	arm::instr *code_p = static_cast<arm::instr *>(machine_code);
	const auto code_p_begin = code_p;

	// now we can write our function prologue in the place of the first and second noops
	code_generator::writeIns(code_p++, push);
	// get the machine_code pointer by pointing at the push instruction
	constexpr arm::instr get_machine_code = arm::op(arm::AL, arm::SUB, rmachine_code, arm::pc, 8 + 4) | arm::op_I;
	code_generator::writeIns(code_p++, get_machine_code);
	// save the arguments in callee-saved registers
	#if 0
	for (int x: {0, 1, 2})
	{
		code_generator::writeIns(code_p++, arm::movr(x + 4, x));
	}
	#else // do it in 2 instructions but with the stack
	constexpr arm::instr push_args = arm::push({0, 1, 2});
	constexpr arm::instr pop_args = arm::pop({rmemory, rregs, rcallbacks});
	code_generator::writeIns(code_p++, push_args);
	code_generator::writeIns(code_p++, pop_args);
	#endif
	code_generator code_gen{code_p};
	code_gen.movWord(rmax, 0x7fff);
	// jump to the right instruction according to pc (loaded from the saved state)
	code_gen.condJump(arm::AL, code_p - code_p_begin, 0, pc < 2 ? 2 : pc);

	for (word i = 2; i <= max; ++i)
	{
		code_p = code_p_begin + INSTR_PER_WORD * i;
		code_gen.at(i);
	}

	callbacks.write_mem = write_mem_impl<code_generator, arm::instr>;
}

#endif // __arm__
