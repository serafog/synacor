#include<initializer_list>
#include<iostream>
#include<fstream>
#include<cstdint>
#include<cstring>
#include<cstdlib>
#include<string>
#include<cstddef>
#include<stack>
#include<vector>
#include<cassert>
#include<algorithm>
#include<charconv>
#include<sys/mman.h>
typedef std::uint16_t word;
constexpr word max = 0x7fff;
extern "C"
{
word memory[max+1] = {};
word regs[8] = {};
}
static struct wordStack: std::stack<word>
{
	using std::stack<word>::c; // the underlying container
} stack;

// to check that jump targets point to correctly compiled instructions
std::vector<bool> instruction_start(max + 1);
	
#define SAVESTATE_BIN "savestate.bin"

static void saveState(const word pc)
{
	std::ofstream out(SAVESTATE_BIN, std::ios::binary);
	auto putWord = [&out](const word w)
	{
		out.put(w & 0xff).put(w >> 8);
	};
	for (word const w: memory)
	{
		putWord(w);
	}
	for (word const reg: regs)
	{
		putWord(reg);
	}
	putWord(pc);
	for (word const w: stack.c)
	{
		putWord(w);
	}
}

static bool loadState(word &pc)
{
	std::ifstream in(SAVESTATE_BIN, std::ios::binary);
	if (not in) return false;
	auto getWord = [&in]() -> word
	{
		char x, y;
		if (in.get(x).get(y)) return (unsigned char)x | (unsigned char)y << 8;
		else return 0xf000;
	};
	for (word &w: memory)
	{
		w = getWord();
	}
	for (word &reg: regs)
	{
		reg = getWord();
	}
	pc = getWord();
	word w;
	while ((w = getWord()) != 0xf000)
	{
		stack.push(w);
	}
	return true;
}

enum instruction
{
	i_halt,
	i_set,
	i_push,
	i_pop,
	i_eq,
	i_gt,
	i_jmp,
	i_jt,
	i_jf,
	i_add,
	i_mult,
	i_mod,
	i_and,
	i_or,
	i_not,
	i_rmem,
	i_wmem,
	i_call,
	i_ret,
	i_out,
	i_in,
	i_noop
};

extern "C"
{ // put the interface with machine code here, to disable C++ complications if ever

enum Errors
{
	// ARM immediate fields support one byte worth of non-zero bits
	// anything after 0x8008 is invalid according to the challenge
	err_halt = 0xff00
};

// Do not let exceptions pass into our machine code.
// Non-throwing functions are permitted to call potentially-throwing functions. Whenever an exception is thrown and the search for a handler encounters the outermost block of a non-throwing function, the function std::terminate [or std::unexpected (until C++17)] is called.
void putchar_impl(word w) noexcept
{
	const auto c = static_cast<unsigned char>(w);
	std::cout << c;
	// don't flush here since cout will be flushed when cin reads
}

void write_mem_impl(word addr, word value) noexcept;

word getchar_impl(word pc) noexcept
{
// pc can just be computed by the JIT compiler statically at the location of the i_in instruction
// that is to say the machine code doesn't need to track pc everywhere.
	char c;
	if (std::cin.get(c)) // input was ok
	{
		static bool seenNewLine = false;
#if 0
		static std::ofstream log("input.log");
		log << c;
#endif
		if (seenNewLine and c == '!')
		{ // special command follows
			std::string line;
			if (std::getline(std::cin, line))
			{
				if (line == "prepare teleporter")
				{
	// set 6 into the first register instead, the expected result from ack
					memory[5483 + 2] = 6;
					write_mem_impl(5483, memory[5483]); // recompile the corresponding instruction
	// replace the call to ack with noops
					memory[5489] = i_noop;
					write_mem_impl(5489, i_noop);
					memory[5489 + 1] = i_noop;
					write_mem_impl(5489 + 1, i_noop);
#ifdef __arm__
	// set the eigth register to the correct teleporter value
					regs[7] = 25734;
#else
	// i_in does not reload the registers so instead
	// we need to bypass 5451: jf $7 5605 while our 8th register isn't set yet
	// and make a new set $7 25734 instruction in its place:
					memory[5451 + 0] = i_set;
					memory[5451 + 2] = 25734;
					write_mem_impl(5451, memory[5451]);
#endif
					std::cout << "\nPrepared.\n";
				}
			}
#if 0
			log << line << '\n';
#endif
			c = '\n'; // hide this line to the program
		}
		seenNewLine = c == '\n';
		return static_cast<unsigned char>(c);
	}
	else // probably EOF
	{
		saveState(pc); // restart i_in when loading state
		return err_halt;
	}
}

void stack_push_impl(word w) noexcept
{
	stack.push(w);
}

word stack_pop_impl(void) noexcept
{
	if (not stack.empty())
	{
		const word w = stack.top();
		stack.pop();
		return w;
	}
	else
	{
		std::cerr << "Error: empty stack\n";
		return err_halt;
	}
}

word stack_pop_or_halt_impl(void) noexcept
{
	if (not stack.empty())
	{
		const word w = stack.top();
		stack.pop();
		return w;
	}
	else
	{
		std::exit(3);
	}
}

word modulo_impl(word a, word b) noexcept
{
	return a % b;
}

static void* machine_code;
constexpr int INSTR_PER_WORD = 16; // a word is either an instruction or an argument, so instructions with arguments get more space
constexpr auto CODE_SIZE = (max+1) * INSTR_PER_WORD;
constexpr auto CODE_SIZE_IN_BYTES = CODE_SIZE * sizeof(std::uint32_t);

word check_jump_target_impl(word w) noexcept
{
	if (not instruction_start[w])
	{
		std::cerr << "Error: jump to non-instruction at " << w << '\n';
		std::ofstream machine_code_out("machine_code.bin", std::ios::binary);
		machine_code_out.write((const char*)machine_code, CODE_SIZE_IN_BYTES);
		std::ofstream memory_out("memory.bin", std::ios::binary);
		memory_out.write((const char*)memory, sizeof memory);
		std::exit(4);
	}
	return w; // leave the address in r0/a0 for the caller
}

static struct Callbacks
{
	void (*putchar)(word) noexcept;
	word (*getchar)(word pc) noexcept;
	void (*stack_push)(word) noexcept;
	word (*stack_pop)(void) noexcept;
	word (*stack_pop_or_halt)(void) noexcept;
	void (*write_mem)(word addr, word value) noexcept;
	word (*modulo)(word a, word b) noexcept;
	word (*check_jump_target)(word w) noexcept;
} callbacks = {
	putchar_impl,
	getchar_impl,
	stack_push_impl,
	stack_pop_impl,
	stack_pop_or_halt_impl,
	nullptr,
	modulo_impl,
	check_jump_target_impl,
};

} // extern "C"

void write_mem_impl(word addr, word value) noexcept
{
	callbacks.write_mem(addr, value);
}

template <typename Code_Generator, typename Instr>
void write_mem_impl(word addr, word value) noexcept
{
	// memory[addr] = value; already done in machine code
	if (value <= i_noop)
	{ // look up and compile the equivalent machine code
		auto code_p = static_cast<Instr *>(machine_code) + INSTR_PER_WORD * addr;
		const auto code_p_begin = code_p;
		Code_Generator{code_p}.at(addr);
		__builtin___clear_cache(code_p_begin, code_p);
	}
	else
	{ // check if we are in the args of an instruction and recompile it
		constexpr static std::initializer_list<signed char> n_args{
			0, 2, 1, 1, 3, 3, 1, 2, 2, 3, 3, 3, 3, 3, 2, 2, 2, 1, 0, 1, 1, 0,
		};
		// TODO: should we search starting at dist=3 instead?
		// I feel the answer isn't obvious, it depends on what the actual program counter will be
		for (int dist = 1; dist <= 3; ++dist)
		{
			const auto w = memory[addr - dist];
			if (w <= i_noop and dist <= n_args.begin()[w])
			{ // found an instruction in range

				auto code_p = reinterpret_cast<Instr *>(machine_code) + INSTR_PER_WORD * (addr - dist);
				const auto code_p_begin = code_p;
				Code_Generator{code_p}.at(addr);
				__builtin___clear_cache(code_p_begin, code_p);
				break;
			}
		}
	}
}

static class WillUnmap
{
	void *ptr = 0;
	std::size_t length;
public:
	~WillUnmap()
	{
		if (ptr) munmap(ptr, length);
	}
	void set(void *p, std::size_t len)
	{
		ptr = p;
		length = len;
	}
} willUnmap;

static bool enableCheckJumpTarget = false;
static void configureCheckJumpTarget()
{
	const char *levelStr = std::getenv("VM_JIT_CHECK_JUMP_TARGET");
	if (levelStr)
	{
		int level = 0;
		std::from_chars(levelStr, levelStr + 1, level);
		enableCheckJumpTarget = level;
	}
}

#if 0
gcc -E -dM -x c /dev/null | fgrep -i arm
#define __arm__ 1

gcc -E -dM -x c /dev/null | fgrep -i aarch
#define __aarch64__ 1

gcc -E -dM -x c /dev/null | fgrep -i risc
#define __riscv 1

gcc -E -dM -x c /dev/null | fgrep -i amd
#define __amd64__ 1
#endif

#include "vm_jit_arm.cc"
#include "vm_jit_riscv.cc"

static int start_machine_code()
{
	typedef int (*machine_code_ptr)(word *memory, word *regs, Callbacks *);
	return reinterpret_cast<machine_code_ptr>(machine_code)(memory, regs, &callbacks);
}

static int run(word pc) noexcept
try
{
	void *const code = mmap(0, CODE_SIZE_IN_BYTES,
		PROT_READ | PROT_WRITE | PROT_EXEC,
		MAP_PRIVATE | MAP_ANONYMOUS,
		-1, 0);
	if (code == MAP_FAILED)
	{
		std::cerr << "Failed allocating memory for machine code: " << std::strerror(errno) << '\n';
		return -1;
	}
	willUnmap.set(code, CODE_SIZE_IN_BYTES);
	machine_code = code;

	if (memory[0] != i_noop or memory[1] != i_noop)
	{
		std::cerr << "Broken expectation: the memory doesn't start with 2 noops\n";
		return -2;
	}

	prepare_machine_code(pc);

	__builtin___clear_cache(code, (char*)code + CODE_SIZE_IN_BYTES);
#if 0
	{
		std::ofstream machine_code_out("machine_code.bin", std::ios::binary);
		machine_code_out.write((const char*)code, CODE_SIZE_IN_BYTES);
	}
#endif
	return start_machine_code();
}
catch (const char *err)
{
	std::cerr << err << '\n';
	return -3;
}

int main(int argc, char *argv[])
{
	std::ios::sync_with_stdio(false);
	code_generator::detectCpu();
	configureCheckJumpTarget();

	word pc = 0;
	if (argc < 2)
	{
		if (not loadState(pc))
		{
			std::cerr << "Need either a binary as argument or a saved state.\n";
			return 1;
		}
		// else carry on
	}
	else
	{
		std::ifstream in(argv[1], std::ios::binary);
		auto getWord = [&in]() -> word
		{
			char x, y;
			if (in.get(x).get(y)) return (unsigned char)x | (unsigned char)y << 8;
			else return 0xf000;
		};
		word w;
		word *m = memory;
		while ((w = getWord()) != 0xf000 && m <= &memory[max])
		{
			*m++ = w;
		}
		if (m == memory)
		{
			std::cerr << "Could not read the program in `" << argv[1] << "'\n";
			return 1;
		}
	}

	return run(pc);
}
