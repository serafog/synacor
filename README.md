# synacor
https://challenge.synacor.com/

`vm` is the original VM in C++ with an optional tracing feature.
`vm_jit` supports 32-bit ARM: both ARMv6 and ARMv7, and 64-bit RISC-V: rv64gc.
