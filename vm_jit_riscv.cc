#ifdef __riscv

namespace riscv
{
	typedef std::uint32_t instr;
	enum { zero, ra, sp, gp, tp, t0, t1, t2,
		s0, fp = s0, s1, a0, a1, a2, a3, a4, a5,
		a6, a7, s2, s3, s4, s5, s6, s7,
		s8, s9, s10, s11, t3, t4, t5, t6 };

	constexpr bool isArgReg(int r)
	{
		return r >= a0 && r <= a7;
	}
	
	constexpr instr type_I(int opcode, int rd, int funct3, int rs1, int imm)
	{
		if (imm < -(1 << 11) || imm >= 1 << 12) throw "imm overflow in type_I";
		return opcode | rd << 7 | funct3 << 12 | rs1 << 15 | (imm & 0xfff) << 20;
	}

	constexpr instr type_R(int opcode, int rd, int funct3, int rs1, int rs2, int funct7)
	{
		return type_I(opcode, rd, funct3, rs1, rs2 | funct7 << 5);
	}

	constexpr instr type_S(int opcode, int funct3, int rs1, int rs2, int imm)
	{
		if (imm < -(1 << 11) || imm >= 1 << 12) throw "imm overflow in type_S";
		return type_R(opcode, imm & 0b1'1111, funct3, rs1, rs2, (imm & 0xfe0) >> 5);
	}

	constexpr instr ld(int rd, int rs1, int imm)
	{
		return type_I(0b0000011, rd, 0b011, rs1, imm);
	}

	constexpr instr lh(int rd, int rs1, int imm)
	{
		return type_I(0b0000011, rd, 0b001, rs1, imm);
	}

	constexpr instr sd(int imm, int rs1, int rs2)
	{
		return type_S(0b0100011, 0b011, rs1, rs2, imm);
	}

	constexpr instr sh(int imm, int rs1, int rs2)
	{
		return type_S(0b0100011, 0b001, rs1, rs2, imm);
	}
/*
objdump -d /usr/bin/awk -M numeric | awk '/[ \t]sh[ \t]/ {split($4,a,/[x,)(]+/); print a[1] " " a[2] " " a[3] " " a[4]; printf "static_assert(0x%s == sh(%s, %s, %s));\n", $2, a[3], a[4], a[2] | "sort -u >riscv_sh_tests.hpp"}'
*/
#include "riscv_sh_tests.hpp"

	constexpr instr addi(int rd, int rs1, int imm)
	{
		return type_I(0b0010011, rd, 0b000, rs1, imm);
	}

	constexpr instr mov(int rd, int rs1)
	{
		return addi(rd, rs1, 0);
	}

	constexpr instr xori(int rd, int rs1, int imm)
	{
		return type_I(0b0010011, rd, 0b100, rs1, imm);
	}

	constexpr instr lnot(int rd, int rs1)
	{
		return xori(rd, rs1, -1);
	}

	constexpr instr slli(int rd, int rs1, int shamt)
	{
		if (shamt < 0 || shamt >= 1 << 6) throw "shamt overflow in slli";
		return type_I(0b0010011, rd, 0b001, rs1, shamt);
	}

	constexpr instr sltiu(int rd, int rs1, int imm)
	{
		return type_I(0b0010011, rd, 0b011, rs1, imm);
	}

	constexpr instr add(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b000, rs1, rs2, 0b0000000);
	}

	constexpr instr sub(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b000, rs1, rs2, 0b0100000);
	}

	constexpr instr lor(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b110, rs1, rs2, 0b0000000);
	}

	constexpr instr land(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b111, rs1, rs2, 0b0000000);
	}

	constexpr instr mul(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b000, rs1, rs2, 0b0000001);
	}

	constexpr instr rem(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b110, rs1, rs2, 0b0000001);
	}

	constexpr instr sltu(int rd, int rs1, int rs2)
	{
		return type_R(0b0110011, rd, 0b011, rs1, rs2, 0b0000000);
	}

	constexpr instr type_B(int opcode, int funct3, int rs1, int rs2, int imm)
	{
		if (imm < -(1 << 12) || imm >= 1 << 12) throw "imm overflow in type_B";
		return type_S(opcode, funct3, rs1, rs2,
			(imm & 0b0111'1111'1110) |
			(imm >> 11 & 1) |
			(imm & 0x1000) >> 1);
	}

	constexpr instr beq(int rs1, int rs2, int imm)
	{
		return type_B(0b1100011, 0b000, rs1, rs2, imm);
	}

	constexpr instr bne(int rs1, int rs2, int imm)
	{
		return type_B(0b1100011, 0b001, rs1, rs2, imm);
	}

	constexpr instr bgt(int rs1, int rs2, int imm)
	{
		// this is blt so swap rs1, rs2:
		return type_B(0b1100011, 0b100, rs2, rs1, imm);
	}
/*
objdump -Mno-aliases -d ../adventofcode/2023/1.exe | awk '$3 ~/^b[el][qt]$/ {split($4,a,/,/); sub(/:/,"",$1); print $1 ":" a[1] " " a[2] " " a[3]; swp = $3 == "blt"; printf "static_assert(0x%s == %s(%s, %s, 0x%s - 0x%s));\n", $2, swp?"bgt":$3, a[swp?2:1], a[swp?1:2], a[3], $1 | "sort -u >riscv_b_tests.hpp"}'
*/
#include "riscv_b_tests.hpp"

	constexpr instr type_U(int opcode, int rd, int imm)
	{
		// imm in the instruction is the direct 32-bit signed value
		// but here we're expecting a 20-bit signed integer
		if (imm < -(1 << 20) || imm >= 1 << 20) throw "imm overflow in type_U";
		return opcode | rd << 7 | (imm & 0xf'ffff) << 12;
	}

	constexpr instr type_J(int opcode, int rd, int imm)
	{
		if (imm < -(1 << 21) || imm >= 1 << 21) throw "imm overflow in type_J";
		return type_U(opcode, rd,
			(imm >> 12 & 0xff) | (imm >> 11 & 1) << 8 |
			(imm >> 1 & 0x3ff) << 9 | (imm & 0x10'0000) >> 1);
	}

	constexpr instr jal(int rd, int imm)
	{
		return type_J(0b1101111, rd, imm);
	}

	constexpr instr jal(int imm)
	{
		return jal(ra, imm);
	}
/*
objdump -d /bin/sh -M numeric,no-aliases | awk '/[ \t]jal[ \t]/ {split($4,a,/[x,)(]+/); sub(/:/,"",$1); print $1 ":" a[1] " " a[2] " " a[3]; printf "static_assert(0x%s == jal(%s, 0x%s - 0x%s));\n", $2, a[2], a[3], $1 | "sort -u >riscv_jal_tests.hpp"}'
*/
#include "riscv_jal_tests.hpp"

	constexpr instr jalr(int rd, int rs1, int imm)
	{
		return type_I(0b1100111, rd, 0b000, rs1, imm);
	}

	constexpr instr lui(int rd, int imm)
	{
		return type_U(0b0110111, rd, imm >> 12);
	}

	constexpr instr auipc(int rd, int imm)
	{
		return type_U(0b0010111, rd, imm >> 12);
	}

	constexpr std::uint16_t type_CSS(int op, int rs2, int imm, int funct3)
	{
		if (imm < 0 || imm >= 1 << 6) throw "imm overflow in type_CSS";
		return op | rs2 << 2 | imm << 7 | funct3 << 13;
	}

	constexpr std::uint16_t c_sdsp(int rs2, int uimm)
	{
		if (uimm < 0 || uimm > 0770) throw "uimm overflow in c_sdsp";
		return type_CSS(0b10, rs2, (uimm & 070) | uimm >> 6, 0b111);
	}
/*
objdump -d /bin/sh -M numeric | awk '/sd[ \t]+[^f].*x2\)/ {split($4,a,/,/); split(a[2],b,/\(/); print $2 "\t" a[1] "\t" b[1]; printf "static_assert(0x%s == c_sdsp(%s, %s));\n", $2, substr(a[1],2), b[1] |"sort |uniq >riscv_sdsp_tests.hpp"}'
*/
#include "riscv_sdsp_tests.hpp"

	constexpr instr sdsp_pair(int rs1, int rs2, int uimm)
	{
		return c_sdsp(rs1, uimm) | c_sdsp(rs2, uimm + 8) << 16;
	}

	constexpr std::uint16_t type_CI(int op, int imm, int r, int funct3)
	{
		if (imm < 0 || imm >= 1 << 6) throw "imm overflow in type_CI";
		return op | (imm & 0b1'1111) << 2 | r << 7 | (imm & 0b10'0000) << (12 - 5) | funct3 << 13;
	}

	constexpr std::uint16_t c_ldsp(int rd, int uimm)
	{
		if (uimm < 0 || uimm > 0770) throw "uimm overflow in ldsp";
		return type_CI(0b10, uimm >> 6 | (uimm & 0b11'1000), rd, 0b011);
	}
/*
objdump -d /bin/sh -M numeric | awk '/ld[ \t]+[^f].*x2\)/ {split($4,a,/,/); split(a[2],b,/\(/); print $2 "\t" a[1] "\t" b[1]; printf "static_assert(0x%s == c_ldsp(%s, %s));\n", $2, substr(a[1],2), b[1] |"sort |uniq >riscv_ldsp_tests.hpp"}'
*/
#include "riscv_ldsp_tests.hpp"

	constexpr instr ldsp_pair(int rd1, int rd2, int uimm)
	{
		return c_ldsp(rd1, uimm) | c_ldsp(rd2, uimm + 8) << 16;
	}
}

constexpr int rmemory = riscv::s1, rcallbacks = riscv::s2, rmachine_code = riscv::s3;
constexpr int rreg0 = riscv::s4, rreg7 = riscv::s11; // leave s4 to s11 for the machine 8 registers.
constexpr int reg_sp_offset = 1;
static_assert(rreg7 - rreg0 + 1 == 8);

struct code_generator
{
	riscv::instr* &code_p;

	void ret()
	{
		for (int r = riscv::s10; r >= riscv::s2; r -= 2)
		{
			*code_p++ = riscv::ldsp_pair(r, r + 1, (r - riscv::s2 + 4) * int(sizeof(void*)));
		}
		*code_p++ = ldsp_pair(riscv::s0, riscv::s1, 0);
		*code_p++ = ld(riscv::ra, riscv::sp, 15 * int(sizeof(void*)));
		*code_p++ = riscv::addi(riscv::sp, riscv::sp, 16 * int(sizeof(void*)));
		*code_p++ = riscv::jalr(riscv::zero, riscv::ra, 0);
	}

	void haltIf_err_halt()
	{
		movWord(riscv::t6, err_halt);
		const auto branch_code_p = code_p++;
		ret();
		*branch_code_p = riscv::bne(riscv::t6, riscv::a0, (code_p - branch_code_p) * int(sizeof(*code_p)));
	}

	void movWord(const int dst, const word val)
	{
		if (val >= 1 << 11)
		{
			*code_p++ = riscv::lui(dst, val + 0x1000 * ((val & 0x800) != 0));
			*code_p++ = riscv::addi(dst, dst, val & 0xfff);
		}
		else
			*code_p++ = riscv::addi(dst, riscv::zero, val);
	}
	
	int readArg(const int dst, const word src)
	{
		if (src > max && riscv::isArgReg(dst))
		{ // must move to argument registers
			const int r = src - max - 1 + rreg0;
			*code_p++ = riscv::mov(dst, r);
			return dst;
		}
		else if (src > max)
		{ // skip temporary registers
			const int r = src - max - 1 + rreg0;
			return r;
		}
		else
		{ // move an immediate value either way
			movWord(dst, src);
			return dst;
		}
	}

	void writeReg(const word dst, const int src)
	{
		if (dst > max)
			*code_p++ = riscv::mov(dst - max - 1 + rreg0, src);
	}

	int writableReg(const word dst)
	{
		if (dst > max)
			return dst - max - 1 + rreg0;
		else
			return 0;
	}

	void ternary(word &i, const bool mask, riscv::instr oper(int, int, int))
	{
		const word a = memory[++i];
		const word b = memory[++i];
		const word c = memory[++i];
		const int t0 = readArg(riscv::t0, b);
		const int t1 = readArg(riscv::t1, c);
		const int t2 = writableReg(a);
		*code_p++ = oper(t2, t0, t1);
		if (mask)
		{
			movWord(riscv::t5, max);
			*code_p++ = riscv::land(t2, t2, riscv::t5);
		}
	}

	void at(word &i)
	{
		const auto i_begin = i;
		const word w = memory[i];
		const auto code_p_begin = code_p;
		switch (w)
		{
			default: // special halt on invalid or unknown instruction
				*code_p++ = 0;
				break;
			case i_halt: // 0 arguments: code size is INSTR_PER_WORD
				*code_p++ = riscv::addi(riscv::a0, riscv::zero, 0);
				ret();
				break;
			case i_noop: // 0 arguments
				break;
			case i_out: // 1 argument: code size is 2 * INSTR_PER_WORD
			{
				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, putchar));
				const word w = memory[++i];
				readArg(riscv::a0, w);
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
				break;
			}
			case i_jt: // 2 arguments: code size is 3 * INSTR_PER_WORD
			case i_jf: // 2 arguments
			{
				const auto pc = i;
				const word a = memory[++i];
				const word b = memory[++i];
				const int t0 = readArg(riscv::t0, a);

				if (b <= max)
				{
					const int distance = (code_p_begin - code_p + (b - pc) * INSTR_PER_WORD) * int(sizeof(riscv::instr));
					if (distance < -(1 << 12) || distance >= 1 << 12)
					{ // long jump
						*code_p++ = (w == i_jt ? riscv::beq : riscv::bne)(riscv::zero, t0, 2 * int(sizeof(riscv::instr)));
						*code_p++ = riscv::jal(riscv::zero, distance - int(sizeof(riscv::instr)));
					}
					else
					{ // short jump
						*code_p++ = (w == i_jf ? riscv::beq : riscv::bne)(riscv::zero, t0, distance);
					}
				}
				else
				{ // register jump
					*code_p++ = (w == i_jt ? riscv::beq : riscv::bne)(riscv::zero, t0, 4 * int(sizeof(riscv::instr)));
					*code_p++ = riscv::slli(riscv::t1, b - max - 1 + rreg0, 6);
					*code_p++ = riscv::add(riscv::t2, riscv::t1, rmachine_code);
					*code_p++ = riscv::jalr(riscv::zero, riscv::t2, 0);
				}
				break;
			}
			case i_set: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				writeReg(a, readArg(riscv::t0, b));
				break;
			}
			case i_add: // 3 arguments: code size is 4 * INSTR_PER_WORD
			case i_and: // 3 arguments
			case i_or: // 3 arguments
			case i_mult: // 3 arguments
			case i_mod: // 3 arguments
				switch (w)
				{
				case i_add: ternary(i, true, riscv::add); break;
				case i_and: ternary(i, false, riscv::land); break;
				case i_or: ternary(i, false, riscv::lor); break;
				case i_mult: ternary(i, true, riscv::mul); break;
				case i_mod: ternary(i, false, riscv::rem); break;
				}
				break;
			case i_eq: // 3 arguments
			case i_gt: // 3 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				const word c = memory[++i];
				const int t0 = readArg(riscv::t0, b);
				const int t1 = readArg(riscv::t1, c);
				const int t3 = writableReg(a);
				if (w == i_eq)
				{
					*code_p++ = riscv::sub(riscv::t2, t1, t0);
					*code_p++ = riscv::sltiu(t3, riscv::t2, 1);
				}
				else // i_gt
				{
					*code_p++ = riscv::sltu(t3, t1, t0);
				}
				break;
			}
			case i_push: // 1 argument
			{
				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, stack_push));
				const word w = memory[++i];
				readArg(riscv::a0, w);
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
				break;
			}
			case i_pop: // 1 argument
			case i_ret: // 0 arguments, code size is only INSTR_PER_WORD
			{
				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, stack_pop));
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
				if (w == i_pop)
				{
					haltIf_err_halt();
					const word a = memory[++i];
					writeReg(a, riscv::a0);
				}
				else // i_ret
				{
					*code_p++ = riscv::slli(riscv::t0, riscv::a0, 6);
					*code_p++ = riscv::add(riscv::t1, riscv::t0, rmachine_code);
					*code_p++ = riscv::jalr(riscv::zero, riscv::t1, 0);
				}
				break;
			}
			case i_not: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				const int t0 = readArg(riscv::t0, b);
				*code_p++ = riscv::lnot(riscv::t1, t0);
				movWord(riscv::t5, max);
				const int t2 = writableReg(a);
				*code_p++ = riscv::land(t2, riscv::t1, riscv::t5);
				break;
			}
			case i_call: // 1 argument
			{
				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, stack_push));
				const word pc_next = i + 2;
				movWord(riscv::a0, pc_next);
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
			} // fall-through
			case i_jmp: // 1 argument
			{
				static_assert(INSTR_PER_WORD * sizeof(riscv::instr) == 1 << 6); // confirm sll 6 works
				const word w = memory[++i];
				if (w > max)
				{
					*code_p++ = riscv::slli(riscv::a0, w - max - 1 + rreg0, 6);
					*code_p++ = riscv::add(riscv::a1, riscv::a0, rmachine_code);
					*code_p++ = riscv::jalr(riscv::zero, riscv::a1, 0);
				}
				else
				{
					const auto jump = riscv::jal(riscv::zero, (code_p_begin - code_p + (w - i_begin) * INSTR_PER_WORD) * int(sizeof(riscv::instr)));
					*code_p++ = jump;
				}
				break;
			}
			case i_rmem: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				if (b > max)
				{
					static_assert(sizeof *memory == 2); // confirm lsl(1) works
					const int t0 = readArg(riscv::t0, b);
					*code_p++ = riscv::slli(riscv::t1, t0, 1);
				}
				else // if b is an immediate value, we can precompute sll 1
				{
					movWord(riscv::t1, b << 1);
				}
				*code_p++ = riscv::add(riscv::t2, rmemory, riscv::t1);
				const int t3 = writableReg(a);
				*code_p++ = riscv::lh(t3, riscv::t2, 0);
				break;
			}
			case i_wmem: // 2 arguments
			{
				const word a = memory[++i];
				const word b = memory[++i];
				const int a0 = readArg(riscv::a0, a);
				const int a1 = readArg(riscv::a1, b);
				*code_p++ = riscv::slli(riscv::t0, a0, 1);
				*code_p++ = riscv::add(riscv::t1, rmemory, riscv::t0);
				*code_p++ = riscv::sh(0, riscv::t1, a1);

				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, write_mem));
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
				break;
			}
			case i_in: // 1 argument: code size is 2 * INSTR_PER_WORD
			{
				// save rreg0 to rreg7 to memory for saveState to find them
				*code_p++ = riscv::ld(riscv::a1, riscv::sp, reg_sp_offset * int(sizeof(void*)));
				for (int rreg = rreg0; rreg <= rreg7; ++rreg)
				{
					*code_p++ = riscv::sh((rreg - rreg0) * int(sizeof(word)), riscv::a1, rreg);
				}
				*code_p++ = riscv::ld(riscv::ra, rcallbacks, offsetof(Callbacks, getchar));
				// pass the program counter as the argument (in order to savestate on EOF)
				movWord(riscv::a0, i);
				*code_p++ = riscv::jalr(riscv::ra, riscv::ra, 0);
				haltIf_err_halt();
				writeReg(memory[++i], riscv::a0);
				break;
			}
		}
		const auto code_p_end = code_p_begin + (i + 1 - i_begin) * INSTR_PER_WORD;
		// there will be a gap, write a branch to the next block
		// only if the instruction isn't a guaranteed jump elsewhere
		constexpr static std::initializer_list<word> always_jumps{i_halt, i_jmp, i_call, i_ret};
		// w > i_noop means illegal instruction
		if (w <= i_noop && std::find(always_jumps.begin(), always_jumps.end(), w) == always_jumps.end())
		{
			const auto jump = riscv::jal(riscv::zero, (code_p_begin - code_p + (i + 1 - i_begin) * INSTR_PER_WORD) * int(sizeof(riscv::instr)));
			*code_p++ = jump;
		}
		assert(code_p <= code_p_end);
		// null instructions, 0x0000'0000, are invalid so we always need to jump over them
	}

	static void detectCpu()
	{
	}
};

static void prepare_machine_code(word pc)
{
	auto code_p = static_cast<riscv::instr *>(machine_code);
	const auto code_p_begin = code_p;

	// now we can write our function prologue in the place of the first and second noops
	*code_p++ = riscv::addi(riscv::sp, riscv::sp, -16 * int(sizeof(void*)));
	*code_p++ = riscv::sd(15 * int(sizeof(void*)), riscv::sp, riscv::ra);
	*code_p++ = riscv::sd(14 * int(sizeof(void*)), riscv::sp, riscv::fp);
	*code_p++ = sdsp_pair(riscv::s0, riscv::s1, 0);
	*code_p++ = riscv::addi(riscv::fp, riscv::sp, 16 * int(sizeof(void*)));
	for (int r = riscv::s2; r < riscv::s11; r += 2)
	{
		*code_p++ = riscv::sdsp_pair(r, r + 1, (r - riscv::s2 + 4) * int(sizeof(void*)));
	}
	const int extra = code_p - code_p_begin;
	*code_p++ = riscv::auipc(rmachine_code, 0);
	*code_p++ = riscv::addi(rmachine_code, rmachine_code, extra * -int(sizeof(riscv::instr)));
	*code_p++ = riscv::mov(rmemory, riscv::a0);
	*code_p++ = riscv::sd(reg_sp_offset * int(sizeof(void*)), riscv::sp, riscv::a1);
	*code_p++ = riscv::mov(rcallbacks, riscv::a2);
	for (int rreg = rreg0; rreg <= rreg7; ++rreg)
	{
		*code_p++ = riscv::lh(rreg, riscv::a1, (rreg - rreg0) * int(sizeof(word)));
	}

	code_generator code_gen{code_p};

	// jump to the right instruction according to pc (loaded from the saved state)
	const auto jump = riscv::jal(riscv::zero, (code_p_begin - code_p + (pc < 2 ? 2 : pc) * INSTR_PER_WORD) * int(sizeof(riscv::instr)));
	*code_p++ = jump;

	for (word i = 2; i <= max; ++i)
	{
		code_p = code_p_begin + INSTR_PER_WORD * i;
		code_gen.at(i);
	}

	callbacks.write_mem = write_mem_impl<code_generator, riscv::instr>;
}

#endif // __riscv
